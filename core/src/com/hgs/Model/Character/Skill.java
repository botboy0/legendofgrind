package com.hgs.Model.Character;

import com.hgs.Model.LOG;

public class Skill {
    final LOG log;
    final String name;
    int level=1;
    final double basexp=10;
    final double moneygrowth=1.05;
    final double xpgrowth=moneygrowth;
    final double xpnextgrowth= 1.1;
    double xp;
    double xpnext=getnextlvlxp();

    public Skill(LOG log,String name){
        this.name=name;
        this.log=log;
    }

    public void save(){
        String skilldata=level+",";
        long xpdata = Double.doubleToRawLongBits(getxp());
        skilldata+=xpdata;
        LOG.data.putString(name,skilldata);
    }

    public void load(){
        String[] skilldata= LOG.data.getString(name).split(",");
        if (skilldata.length==1)return;
        this.level=Integer.parseInt(skilldata[0]);
        this.xpnext=getnextlvlxp();
        this.xp= Double.longBitsToDouble(Long.parseLong(skilldata[1]));
    }

    public void addxp(double xp) {
        this.xp +=xp;
        while (this.xp>=xpnext){
            levelup();
        }
        log.player.save();
    }

    void levelup(){
        xp-=xpnext;
        level++;
        xpnext=getnextlvlxp();
    }


    public double getxp() {
        return xp;
    }
    public double getxpnext() {
        return xpnext;
    }
    public int getlevel() {
        return level;
    }
    public double getnextlvlxp(){
        return basexp*Math.pow(xpnextgrowth,level-1);
    }
    public double getmoneymult(){
        return Math.pow(moneygrowth,level-1);
    }
    public double getxpmult(){
        return Math.pow(xpgrowth,level-1);
    }

}
