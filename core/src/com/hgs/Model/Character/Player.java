package com.hgs.Model.Character;

import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Inventory.InventoryModel;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.LOG;

public class Player {
    private final LOG log;
    private double balance;
    private final Skill farming;
    private final Skill mining;
    private final Skill exploration;
    private final Skill crafting;
    private final Array<Skill>skills;
    private final InventoryModel inventorymodel;
    private final InventoryModel seedmodel;
    private final InventoryModel hoemodel;
    private final InventoryModel pickaxemodel;
    private final Array<InventoryModel>models;

    public Player(LOG log){
        inventorymodel=new InventoryModel(log,"player", 100);

        seedmodel= new InventoryModel(log,"seed","Seed",1);
        hoemodel = new InventoryModel(log,"hoe","Hoe",1);
        pickaxemodel= new InventoryModel(log,"pickaxe","Pickaxe",1);

        farming = new Skill(log,"farming");
        mining = new Skill(log,"mining");
        exploration = new Skill(log,"exploration");
        crafting = new Skill(log,"crafting");


        skills = new Array<Skill>();
        skills.add(farming,mining,exploration,crafting);

        models= new Array<InventoryModel>();
        models.add(inventorymodel,seedmodel, hoemodel,pickaxemodel);

        this.log=log;
    }

    public void save(){
        LOG.data.putLong("balance",Double.doubleToRawLongBits(this.balance));
        for (Skill skill:skills)skill.save();
        for (InventoryModel model:models)model.save();
        log.farmingscene.farmarray.save();
        log.miningscene.ore.save();
        log.explorescene.explorepanel.save();
        log.craftscene.save();
        LOG.data.flush();

    }


    public void load() throws InstantiationException, IllegalAccessException {
        this.balance= Double.longBitsToDouble(LOG.data.getLong("balance",0));
        for (Skill skill:skills)skill.load();
        for (InventoryModel model:models)model.load();
        log.farmingscene.farmarray.load();
        log.miningscene.ore.load();
        log.craftscene.load();
    }


    public void addbalance(double balance) {
        this.balance+=balance;
        save();
    }

    public void substractbalance(double balance) {
        this.balance -= balance;
        save();
    }

    public double getbalance() {
        return balance;
    }

    public Skill getFarming() {
        return farming;
    }

    public Skill getMining() {
        return mining;
    }

    public Skill getExploration() {
        return exploration;
    }

    public Skill getCrafting() {
        return crafting;
    }

    public InventoryModel getInventorymodel() {
        return inventorymodel;
    }

    public InventoryModel getSeedmodel() {
        return seedmodel;
    }

    public InventoryModel getHoemodel() {
        return hoemodel;
    }

    public Array<InventoryModel> getModels() {
        return models;
    }

    public InventoryModel getPickaxemodel() {
        return pickaxemodel;
    }
}
