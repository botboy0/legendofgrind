package com.hgs.Model.Inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.ItemData;
import com.hgs.Model.Inventory.Item.ItemStack;
import com.hgs.Model.Inventory.Item.Items;
import com.hgs.Model.Inventory.Item.Seed.Seed;
import com.hgs.Model.Inventory.Item.Tool.Tool;
import com.hgs.Model.LOG;

public class InventoryModel {
    final LOG log;
    final Array<ItemStack> stacks;
    final Array<Inventory>syncedinventories;
    final int slotcount;
    final String modelname;
    String category;
    public InventoryModel(LOG log,String modelname,int slotcount){
        this.slotcount =slotcount;
        this.modelname=modelname;
        this.log=log;
        stacks= new Array<>();
        syncedinventories= new Array<>();
        for (int i=0;i<slotcount;i++)stacks.add(new ItemStack(null,1));
    }

    public InventoryModel(LOG log,String modelname,String category,int slotcount){
        this.slotcount =slotcount;
        this.modelname=modelname;
        this.category=category;
        this.log=log;
        stacks= new Array<>();
        syncedinventories= new Array<>();
        for (int i=0;i<slotcount;i++)stacks.add(new ItemStack(null,1));
    }



    public void addInventory(Inventory inv){
        syncedinventories.add(inv);
        sync();
    }

    public void Swap(InventoryModel model,int sourceindex,int targetindex){
        ItemStack bufferstack= getStacks().get(sourceindex);
        getStacks().set(sourceindex,model.getStacks().get(targetindex));
        model.getStacks().set(targetindex,bufferstack);
        model.sync();
        log.player.save();
        sync();
    }

    public void RemoveItems(Item item, int count){
        int left=count;
        for (ItemStack stack:stacks) {
            if (stack.getItem()!=null&&stack.getItem().getName().equals(item.getName())){
               while (stack.getCount()>0&&left>0){
                   stack.setCount(stack.getCount() - 1);
                   left--;
               }
               if (left==0){
                   sync();
                   log.player.save();
               }
               if (stack.getCount()==0){
                   stack.setItem(null);
                   stack.setCount(0);
                   sync();
                   log.player.save();
                   continue;

               }

            }
        }

    }

    public void AddItems(Item item, int count){
        int left=count;
        for (int i=0; i<stacks.size;i++) {
            if (stacks.get(i).getItem()==null){
                stacks.set(i,new ItemStack(item,1));
                left--;

            }
            if (stacks.get(i).getItem().getName().equals(item.getName())){
                while (stacks.get(i).getCount()<stacks.get(i).getItem().getMaxstacksize()&&left>0){
                    stacks.get(i).setCount(stacks.get(i).getCount() + 1);
                    left--;
                }
                if (left==0){
                    sync();
                    log.player.save();
                    return;
                }
                if (stacks.get(i).getCount()==stacks.get(i).getItem().getMaxstacksize()){
                    continue;

                }

            }
        }

    }


    public int FindEmpty(){
        for (int i =0;i<stacks.size;i++) {
            if (stacks.get(i).getItem()==null)return i;
        }
        return -1;
    }

    public int getSlotcount() {
        return slotcount;
    }

    public Array<ItemStack> getStacks() {
        return stacks;
    }

    public void setStack(int index, ItemStack stack) {
        stacks.set(index,stack);
    }

    public void save(){
        String inventorysave= new String();
        for (int i=0; i<getStacks().size;i++){
            ItemStack stack= getStacks().get(i);
            if (stack.getItem()==null)continue;
            ItemData data = stack.getItem().getData();
            if (data.getFloat("maxdurability",-1)==-1)inventorysave+=i+","+stack.getItem().getName()+","+stack.getCount()+"\n";
            else inventorysave+=i+","+stack.getItem().getName()+","+stack.getCount()+","+data.getFloat("durability",-1)+"\n";
        }
        LOG.data.putString(modelname,inventorysave);

    }

    public void load(){
        String[] slots = LOG.data.getString(modelname).split("\n");
        for (int i=0;i<slots.length;i++){
            String[]itemdata= slots[i].split(",");
            if (itemdata.length==1)continue;
            try {

                int slot = Integer.parseInt(itemdata[0]);
                int count = Integer.parseInt(itemdata[2]);
                Item item =(Items.valueOf(itemdata[1]).getItem());
                ItemData data = item.getData();
                if (data.getFloat("maxdurability",-1)!=-1)item.getData().putFloat("durability",Float.parseFloat(itemdata[3]));
                setStack(slot,new ItemStack(item,count));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        sync();
    }

    public boolean checkStack(Item item,int amount){
        int left= amount;
        for (ItemStack stack:stacks) {
            if (stack.getItem()!=null&&stack.getItem().getName().equals(item.getName())){
                left-=stack.getCount();
            }
        }
        if (left<=0)return true;
    return false;
    }

    public void sync() {
        for (Inventory inv : syncedinventories) {
            for (InventorySlot slot:inv.getSlots()){
                slot.ClearSlot();
            }
            for (int i = 0; i<getSlotcount(); i++){
                if (stacks.get(i).getItem()!=null) inv.slots.get(i).setSlotItem(getStacks().get(i).getItem(), getStacks().get(i).getCount());
            }
        }
    }

    @Override
    public String toString() {
        String inventorystring="";
        for (ItemStack stack:stacks) {
            if (stack.getItem()!=null)inventorystring+= stack.getItem().getName()+" "+stack.getCount()+"\n"+" "+stack.getItem().getImage();
            else inventorystring+= "emptyslot";
        }
        return inventorystring;
    }

    public String getModelname() {
        return modelname;
    }

    public String getCategory() {
        return category;
    }

    public void addslot(){
        stacks.add(new ItemStack(null,1));
    }
}
