package com.hgs.Model.Inventory.Crafting;

public class Process {
    Recipes recipe;
    float progress=0;
    Status status;

    public Process(Recipes recipe) {
        this.recipe = recipe;
    }
    public Status progress(){
       progress++;
       if (progress<recipe.getCraftpoints())status=Status.Craft;
       if (progress==recipe.getCraftpoints())status=Status.Complete;
       if (progress>recipe.getCraftpoints())status=Status.Ended;
       return status;
    }

    public Recipes getRecipe() {
        return recipe;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
        if (progress<recipe.getCraftpoints())status=Status.Craft;
        if (progress==recipe.getCraftpoints())status=Status.Complete;
        if (progress>recipe.getCraftpoints())status=Status.Ended;
    }

    public enum Status {
        Craft,
        Complete,
        Ended,
    }

    public Status getStatus() {
        return status;
    }

}
