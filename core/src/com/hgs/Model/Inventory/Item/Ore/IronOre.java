package com.hgs.Model.Inventory.Item.Ore;

import com.hgs.Model.Inventory.Item.Resource.IronChunk;

public class IronOre extends Ore{
    public IronOre() {
        super("IronOre",50,new IronChunk(),8,8);
    }
}
