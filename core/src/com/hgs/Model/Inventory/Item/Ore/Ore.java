package com.hgs.Model.Inventory.Item.Ore;

import com.hgs.Model.Inventory.Item.Item;

public abstract class Ore extends Item {
    public Ore(String name,float hpmax,Item drop,float moneyvalue,float xpvalue) {
        super(name,100);
        getData().putFloat("hp",hpmax);
        getData().putFloat("hpmax",hpmax);
        getData().putDouble("moneyvalue",moneyvalue);
        getData().putDouble("xpvalue",xpvalue);
        getData().putString("drop",drop.getName());
    }
}
