package com.hgs.Model.Inventory.Item.Ore;

import com.hgs.Model.Inventory.Item.Resource.Diamond;

public class DiamondOre extends Ore {
    public DiamondOre() {
        super("DiamondOre",200,new Diamond(),25,25);
    }
}
