package com.hgs.Model.Inventory.Item.Resource;

import com.hgs.Model.Inventory.Item.Item;

public class GoldChunk extends Item {
    public GoldChunk() {
        super("GoldChunk");
        getData().putString("category","Resource");
    }
}
