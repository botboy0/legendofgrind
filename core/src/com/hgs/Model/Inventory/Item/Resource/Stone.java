package com.hgs.Model.Inventory.Item.Resource;

import com.hgs.Model.Inventory.Item.Item;

public class Stone extends Item {
    public Stone() {
        super("Stone");
        getData().putString("category","Resource");
    }
}
