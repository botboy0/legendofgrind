package com.hgs.Model.Inventory.Item.Resource;

import com.hgs.Model.Inventory.Item.Item;

public class IronChunk extends Item {
    public IronChunk() {
        super("IronChunk");
        getData().putString("category","Resource");
    }
}
