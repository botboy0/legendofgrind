package com.hgs.Model.Inventory.Item.Resource;

import com.hgs.Model.Inventory.Item.Item;

public class Diamond extends Item {
    public Diamond() {
        super("Diamond");
        getData().putString("category","Resource");
    }
}
