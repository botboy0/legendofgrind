package com.hgs.Model.Inventory.Item.Resource;

import com.hgs.Model.Inventory.Item.Item;

public class Wood extends Item {
    public Wood() {
        super("Wood");
        getData().putString("category","Resource");
    }
}
