package com.hgs.Model.Inventory.Item;

import com.hgs.Model.Inventory.Item.Crop.Carrot;
import com.hgs.Model.Inventory.Item.Crop.Potato;
import com.hgs.Model.Inventory.Item.Crop.Wheat;
import com.hgs.Model.Inventory.Item.Ore.DiamondOre;
import com.hgs.Model.Inventory.Item.Ore.GoldOre;
import com.hgs.Model.Inventory.Item.Ore.IronOre;
import com.hgs.Model.Inventory.Item.Ore.StoneOre;
import com.hgs.Model.Inventory.Item.Resource.*;
import com.hgs.Model.Inventory.Item.Seed.CarrotSeed;
import com.hgs.Model.Inventory.Item.Seed.PotatoSeed;
import com.hgs.Model.Inventory.Item.Seed.WheatSeed;
import com.hgs.Model.Inventory.Item.Tool.Hoe;
import com.hgs.Model.Inventory.Item.Tool.Pickaxe;

public enum Items {

    Carrot(new Carrot()),Potato(new Potato()),Wheat(new Wheat()),

    CarrotSeed(new CarrotSeed()),PotatoSeed(new PotatoSeed()),WheatSeed(new WheatSeed()),

    Hoe(new Hoe()),Pickaxe(new Pickaxe()),

    StoneOre(new StoneOre()),IronOre(new IronOre()),GoldOre(new GoldOre()),DiamondOre(new DiamondOre()),

    Stone(new Stone()),IronChunk(new IronChunk()),GoldChunk(new GoldChunk()),Diamond(new Diamond()),Wood(new Wood());


    private final Item item;

    Items(Item item) {
        this.item=item;
    }

    public Item getItem() {
        try {
            return (Item) getItemClass().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Class getItemClass() {
        return item.getClass();
    }
}
