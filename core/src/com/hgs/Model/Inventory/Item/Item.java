package com.hgs.Model.Inventory.Item;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.hgs.Model.LOG;


public abstract class Item {
    private final ItemData data= new ItemData();
    private final String name;
    private final TextureRegion image;
    private final int maxstacksize;


    public Item(String name) {
        this.name = name;
        this.image = new TextureRegion(LOG.skin.getRegion(name));
        this.maxstacksize = 100;
    }


    public Item(String name, int maxstacksize) {
        this.name = name;
        this.image = new TextureRegion(LOG.skin.getRegion(name));
        this.maxstacksize = maxstacksize;
    }

    public Item(Item item) {
        this.name= item.name;
        this.image= item.image;
        this.maxstacksize = item.maxstacksize;
        data.copydata(item.data.getdata());
    }


    public String getName() {
        return name;
    }

    public TextureRegion getImage() {
        return image;
    }

    public int getMaxstacksize() {
        return maxstacksize;
    }

    public ItemData getData() {
        return data;
    }
}
