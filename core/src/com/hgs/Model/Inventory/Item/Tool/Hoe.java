package com.hgs.Model.Inventory.Item.Tool;

public class Hoe extends Tool {
    public Hoe() {
        super("Hoe", 100);
        getData().putString("category","Hoe");
    }
}
