package com.hgs.Model.Inventory.Item.Tool;

public class Pickaxe extends Tool {
    public Pickaxe() {
        super("Pickaxe", 1000);
        getData().putString("category","Pickaxe");
        getData().putFloat("damage",1f);
    }
}
