package com.hgs.Model;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Inventory.InventoryModel;
import com.hgs.Model.Inventory.Item.Crop.Crop;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.ItemStack;
import com.hgs.Model.Inventory.Item.Items;
import com.hgs.Model.Inventory.Item.Resource.Wood;
import com.hgs.Model.Inventory.Item.Seed.CarrotSeed;
import com.hgs.Model.Inventory.Item.Seed.PotatoSeed;
import com.hgs.Model.Inventory.Item.Seed.Seed;
import com.hgs.Model.Inventory.Item.Seed.WheatSeed;
import com.hgs.Model.Inventory.Item.Tool.Hoe;
import com.hgs.Model.Inventory.Item.Tool.Pickaxe;
import com.hgs.Model.Inventory.Item.Tool.Tool;
import com.hgs.Model.Scenes.*;
import com.hgs.Model.Screens.GameScreen;
import com.hgs.Model.UI.UIFarm;
import com.hgs.Model.Util.Formatting;


public class LOG extends Game {
	public static Preferences data;
	public static int oWIDTH;
	public static int oHEIGHT;
	public static int WIDTH;
	public static int HEIGHT;
	public static Formatting formatting;
	public static BitmapFont font;
	public static Skin skin;
	public static TextureAtlas UIAtlas;
	public static TextureAtlas ItemAtlas;
	public static TextureAtlas ParticleAtlas;
	public static SpriteBatch batch;
	public static GameScreen gs;
	public Array<Scene> scenes;
	public SceneType activescene;
	public FarmingScene farmingscene;
	public MiningScene miningscene;
	public ExploreScene explorescene;
	public CraftScene craftscene;
	public ItemChoiceScene itemchoicescene;
	public MenuScene menuscene;
	public Player player;

	@Override
	public void create () {
		oWIDTH = 1080;
		oHEIGHT = 1080;
		WIDTH = Gdx.graphics.getWidth();
		HEIGHT = Gdx.graphics.getHeight();
		data = Gdx.app.getPreferences("log");
		formatting= new Formatting();
		scenes=new Array<Scene>();
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		font= new BitmapFont(Gdx.files.internal("font.fnt"));
		font.getData().setScale((WIDTH/(float)oWIDTH+HEIGHT/(float)oHEIGHT)*(HEIGHT/(float)WIDTH)/5f);
		font.getData().markupEnabled=true;
		skin = new Skin(Gdx.files.internal("ninepatch.json"));

		UIAtlas = new TextureAtlas("GUI.pack");
		ItemAtlas = new TextureAtlas("Item.pack");
		ParticleAtlas = new TextureAtlas("Particle.pack");
		skin.add("font",font);
		skin.addRegions(UIAtlas);
		skin.addRegions(ItemAtlas);
		skin.addRegions(ParticleAtlas);
		batch= new SpriteBatch();
		player= new Player(this);
		farmingscene = new FarmingScene(this);
		miningscene = new MiningScene(this);
		explorescene = new ExploreScene(this);
		craftscene = new CraftScene(this);
		itemchoicescene= new ItemChoiceScene(this);
		menuscene = new MenuScene(this);
		gs = new GameScreen(this);
		try {
			player.load();
		}
		catch (Exception e){
			e.printStackTrace();
		}


		InventoryModel playermodel = player.getInventorymodel();
//		player.getInventorymodel().AddItemStack(new CarrotSeed(),100);
//		playermodel.AddItemStack(new WheatSeed(),100);
//		playermodel.AddItemStack(new PotatoSeed(),100);
//		playermodel.AddItemStack(new Hoe(),1);
//		playermodel.AddItemStack(new Hoe(),1);
//		playermodel.AddItemStack(new Hoe(),1);
//		playermodel.AddItemStack(new Pickaxe(),1);

		setScreen(gs);
		setScene(SceneType.Menu);



	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void pause() {
		player.save();
	}

	@Override
	public void dispose () {
		batch.dispose();
		gs.getStage().dispose();
	}

	@Override
	public void resize(int width, int height) {
		WIDTH = width;
		HEIGHT = height;
		gs.updateview();
	}

	public void setScene(SceneType type){
		for (Scene scene:scenes){
			scene.hide();
			if (scene.getType()==type)scene.show();
		}
		activescene=type;
	}
}
