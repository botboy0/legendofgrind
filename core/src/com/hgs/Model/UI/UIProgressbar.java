package com.hgs.Model.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.LOG;

public class UIProgressbar extends UIComponent {
    ProgressBar pb;
    final Color color;
    Group progressgroup;
    float fontscale;
    Label progresslabel;
    Container<Label>labelContainer;
    public UIProgressbar(Table table, String name, Vector2 size,Color color,float fontscale) {
        super(table, name, size);
        this.color=color;
        this.fontscale=fontscale;
        setStyle();
    }

    public UIProgressbar(Table table, String name, Vector2 size,Color color, float fontscale, int[] padding) {
        super(table, name, size, padding);
        this.color=color;
        this.fontscale=fontscale;
        setStyle();
    }

    @Override
    void setStyle() {
        progressgroup = new Group();
        progresslabel = new Label("", LOG.skin,"font", Color.WHITE);
        NinePatch knob = new NinePatch(LOG.skin.getPatch("progress"),color);
        knob.setColor(color);
        ProgressBar.ProgressBarStyle pbstyle= new ProgressBar.ProgressBarStyle();
        pbstyle.knobBefore= new NinePatchDrawable(knob);
        pbstyle.knobBefore.setMinWidth(1);
        pbstyle.background= new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        pb = new ProgressBar(0,1,0.0001f,false,pbstyle);
        pb.setSize(size.x,size.y);
        progresslabel.setSize(size.x,size.y);
        progresslabel.setAlignment(Align.center);
        labelContainer= new Container<Label>(progresslabel);
        labelContainer.align(Align.center);
        labelContainer.fill();
        progressgroup.addActor(pb);
        progressgroup.addActor(labelContainer);
        actor= progressgroup;
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil((size.x)*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        progresslabel.setFontScale(size.x/200*ratio*fontscale);
        labelContainer.setPosition( (table.getCell(actor).getPrefWidth())/2, table.getCell(actor).getPrefHeight()/2);
        ProgressBar.ProgressBarStyle pbstyle = pb.getStyle();
        pb.setSize((size.x)*ratio,size.y*ratio);

        progresslabel.setSize(size.x,size.y);
        pbstyle.background.setMinWidth(size.x*ratio);
        pbstyle.background.setMinHeight(size.y*ratio);
        pbstyle.knobBefore.setMinWidth(0);
        pbstyle.knobBefore.setMinHeight(size.y*ratio);
        pb.setStyle(pbstyle);
        actor= progressgroup;
    }

    public ProgressBar getPb() {
        return pb;
    }

    public Label getProgresslabel() {
        return progresslabel;
    }
}
