package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.hgs.Model.LOG;

public class FloatingText extends Actor {
    private String text;
    private final long animationDuration;
    private float deltaX;
    private float deltaY;
    private boolean animated = false;
    private long animationStart;
    private boolean done;
    private float textwidth;
    private final BitmapFontCache cache;
    private  final BitmapFont font;
    private final GlyphLayout layout;


    public FloatingText(String text, long animationDuration) {

        this.text = text;
        this.animationDuration = animationDuration;
        font = LOG.font;
        cache = font.getCache();
        layout = new GlyphLayout();
        layout.setText(font,text);
        textwidth = layout.width;

    }

    public void animate() {
        animated = true;
        done=false;
        animationStart = System.currentTimeMillis();
    }

    public boolean isAnimated() {
        return animated;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (animated) {
            float elapsed = System.currentTimeMillis() - animationStart;
            cache.clear();
            cache.addText(text,getX() + deltaX * elapsed / 1000f, getY() + deltaY * elapsed / 1000f);
            if (elapsed/animationDuration<1)cache.setAlphas((float) (parentAlpha * (1 - Math.pow(elapsed / animationDuration,2))));
            else cache.setAlphas(0);
            cache.draw(batch);
            if (elapsed>animationDuration) {
                done= true;
            }
        }
    }


    public void setDeltaX(float deltaX) {
        this.deltaX = deltaX;
    }

    public void setDeltaY(float deltaY) {
        this.deltaY = deltaY;
    }

    public boolean isDone() {
        return done;
    }

    public void setText(String text) {
        this.text = text;
        layout.setText(font,text);
        textwidth = layout.width;
    }

    public float getTextwidth() {
        return textwidth;
    }
}


