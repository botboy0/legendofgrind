package com.hgs.Model.UI;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.hgs.Model.Inventory.Inventory;
import com.hgs.Model.Inventory.InventorySlot;
import com.hgs.Model.LOG;

public class UIInventory extends UIComponent {
    Vector2 dimensions;
    final int slotsize;
    Inventory iv;


    public UIInventory(Table table, String name, Vector2 size, Vector2 dimensions, int slotsize) {
        super(table, name, size);
        this.dimensions= new Vector2();
        this.dimensions=dimensions;
        this.slotsize=slotsize;
        setStyle();
    }

    public UIInventory(Table table, String name, Vector2 size, Vector2 dimensions, int slotsize, int[] padding) {
        super(table, name, size, padding);
        this.dimensions= new Vector2();
        this.dimensions=dimensions;
        this.slotsize=slotsize;
        setStyle();
    }

    @Override
    void setStyle() {
        iv= new Inventory(LOG.skin,dimensions,slotsize);
        actor=iv.getTable();
    }

    @Override
    public void scale(float ratio) {
        for (InventorySlot s: iv.getSlots()) {
            s.scale(ratio);
            iv.getTable().setSize((int)Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
            iv.getTable().getCell(s).size((int) Math.ceil(s.getSlotsize()*ratio),(int)Math.ceil(s.getSlotsize()*ratio));
        }
        table.getCell(iv.getTable()).padTop(padding[0]*ratio);
        table.getCell(iv.getTable()).padRight(padding[1]*ratio);
        table.getCell(iv.getTable()).padBottom(padding[2]*ratio);
        table.getCell(iv.getTable()).padLeft(padding[3]*ratio);
    }

    public Vector2 getDimensions() {
        return dimensions;
    }

    public Inventory getinventory() {
        return iv;
    }
}
