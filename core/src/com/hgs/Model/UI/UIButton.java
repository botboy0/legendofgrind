package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.hgs.Model.LOG;

public class UIButton extends UIComponent {
    TextButton button;
    final String text;
    float scale=1;
    final Drawable background;
    public UIButton(String text, Table table,Drawable background, String name, Vector2 size, float textscale) {
        super(table, name, size);
        this.text=text;
        this.scale=textscale;
        this.background=background;
        setStyle();

    }

    public UIButton(String text, Table table,Drawable background, String name, Vector2 size, float textscale, int[] padding) {
        super(table, name, size, padding);
        this.text=text;
        this.scale=textscale;
        this.background=background;
        setStyle();
    }

    public UIButton(String text, Table table,Drawable background, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        this.text=text;
        this.background=background;
        setStyle();
    }


    @Override
    void setStyle() {
        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.font = LOG.font;
        style.up = background;
        style.down = background;
        button= new TextButton(text,style);
        actor=button;
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        button.getLabel().setFontScale(size.x/200*ratio*scale);
    }
    public void setbackground(String imagename){
        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.font = LOG.font;
        style.up = LOG.skin.getDrawable(imagename);
        style.down = LOG.skin.getDrawable(imagename);
        button.setStyle(style);
        actor=button;
    }

    public TextButton getButton() {
        return button;
    }

    public String getText() {
        return text;
    }
}

