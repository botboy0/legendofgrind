package com.hgs.Model.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.LOG;

public class UITextTitle extends UIComponent {
    Group titlegroup;
    Button menubutton;
    Label titlelabel;
    float fontscale;
    Container<Label> labelContainer;
    public UITextTitle(Table table, String name, Vector2 size, float fontscale) {
        super(table, name, size);
        this.fontscale=fontscale;
        setStyle();
    }

    public UITextTitle(Table table, String name, Vector2 size, float fontscale, int[] padding) {
        super(table, name, size, padding);
        this.fontscale=fontscale;
        setStyle();
    }

    @Override
    void setStyle() {
        titlegroup = new Group();
        Label.LabelStyle style= new Label.LabelStyle();
        style.background = new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        titlelabel = new Label("Menu", LOG.skin,"font", Color.WHITE);
        titlelabel.getStyle().background= style.background;
        Button.ButtonStyle bs = new Button.ButtonStyle();
        bs.up=LOG.skin.getDrawable("menu");
        bs.down=LOG.skin.getDrawable("menu");
        menubutton = new Button(bs);
        menubutton.setSize(size.y,size.y);
        titlelabel.setSize(size.x-size.y,size.y);
        titlelabel.setAlignment(Align.center);
        labelContainer= new Container<>(titlelabel);
        labelContainer.align(Align.center);
        labelContainer.fill();
        titlegroup.addActor(labelContainer);
        titlegroup.addActor(menubutton);
        actor= titlegroup;
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil((size.x)*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        titlelabel.setFontScale(size.x/200*ratio*fontscale);
        labelContainer.setSize(size.x*ratio,size.y*ratio);
        menubutton.setSize(size.y*ratio,size.y*ratio);
        Label.LabelStyle style = titlelabel.getStyle();
        style.background.setMinWidth((size.x-size.y)*ratio);
        style.background.setMinHeight(size.y*ratio);
        titlelabel.setStyle(style);
        actor= titlegroup;
    }

    public Group getTitlegroup() {
        return titlegroup;
    }

    public Container<Label> getLabelContainer() {
        return labelContainer;
    }

    public Button getMenubutton() {
        return menubutton;
    }
}
