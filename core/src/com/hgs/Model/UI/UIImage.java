package com.hgs.Model.UI;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class UIImage extends UIComponent {
    Image image;
    public UIImage(Table table, Drawable drawable, String name, Vector2 size) {
        super(table, name, size);
        setStyle();
        setImage(drawable);
    }

    public UIImage(Table table,Drawable drawable,String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        setStyle();
        setImage(drawable);
    }

    public UIImage(Table table, String name, Vector2 size) {
        super(table, name, size);
        setStyle();
    }

    public UIImage(Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        setStyle();
    }

    @Override
    void setStyle() {
        image = new Image();
        image.setSize(size.x,size.y);
        actor= image;
    }
    void setImage(Drawable drawable){
        image.setDrawable(drawable);
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
    }

    public Image getImage() {
        return image;
    }
}