package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.LOG;



public class UISelectBox extends UIComponent {
    SelectBox<String> box;
    Array<String> listitems;
    boolean selecting;
    BitmapFont f;
    public UISelectBox(Table table, String name, Vector2 size) {
        super(table, name, size);
        setStyle();
    }

    public UISelectBox(Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        setStyle();
    }

    @Override
    void setStyle() {
        SelectBox.SelectBoxStyle sbstyle= new SelectBox.SelectBoxStyle();
        sbstyle.backgroundOver=new NinePatchDrawable(LOG.skin.getPatch("selectboxbg")) ;
        List.ListStyle liststyle = new List.ListStyle();
        f = new BitmapFont(Gdx.files.internal("font.fnt"));
        liststyle.font= f;
        ScrollPane.ScrollPaneStyle spstyle = new ScrollPane.ScrollPaneStyle();
        spstyle.background=  new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        sbstyle.font=f;
        sbstyle.listStyle=liststyle;
        sbstyle.background= new NinePatchDrawable(LOG.skin.getPatch("selectboxbg"));
        sbstyle.backgroundDisabled= new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        sbstyle.scrollStyle=spstyle;
        sbstyle.listStyle.selection= new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        sbstyle.listStyle.fontColorSelected= new Color(0,1,0,1);
        sbstyle.fontColor= new Color(0,1,0,1);
        sbstyle.listStyle.background=  new NinePatchDrawable(LOG.skin.getPatch("progressbg"));

        box = new SelectBox<String>(sbstyle);

        box.getStyle().background.setLeftWidth(size.x*0.05f);
        box.getStyle().backgroundDisabled.setLeftWidth(size.x*0.05f);
        box.getStyle().backgroundOver.setLeftWidth(size.x*0.05f);
        box.getList().getStyle().selection.setLeftWidth(size.x*0.05f);


        box.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                selecting=true;
            }
        });

        box.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
            selecting=false;
            }
        });


        actor=box;
    }

    @Override
    public void scale(float ratio) {
        box.setSize(size.x*ratio,size.y*ratio);

        f.getData().setScale(size.x*ratio/150);
        box.getScrollPane().validate();


        box.getStyle().background.setLeftWidth(size.x*0.05f*ratio);
        box.getStyle().backgroundDisabled.setLeftWidth(size.x*0.05f*ratio);
        box.getStyle().backgroundOver.setLeftWidth(size.x*0.05f*ratio);
        box.getList().getStyle().selection.setLeftWidth(size.x*0.05f*ratio);


        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);

    }

    public void render(Batch batch){
        batch.begin();
        if (!box.isDisabled()&& selecting)box.getScrollPane().draw(batch,1);
        batch.end();
    }

    public SelectBox<String> getBox() {
        return box;
    }
}
