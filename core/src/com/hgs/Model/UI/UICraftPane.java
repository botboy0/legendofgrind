package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Inventory.Item.ItemStack;
import com.hgs.Model.Inventory.Crafting.Recipes;
import com.hgs.Model.LOG;
import com.hgs.Model.Scenes.CraftScene;
import com.hgs.Model.Util.Formatting;

public class UICraftPane extends UIComponent {
    CraftScene craftscene;
    Recipes recipe;
    Container<Table>container;
    Table t;
    BitmapFont itemfont;
    Label.LabelStyle itemstyle;
    BitmapFont countfont;
    Label.LabelStyle countstyle;
    Label.LabelStyle rewardstyle;
    BitmapFont rewardfont;
    Array<VerticalGroup> groups;
    Image arrow;
    Label rewardlabel;
    Formatting formatter;
    public UICraftPane(CraftScene craftscene,Table table, String name, Vector2 size) {
        super(table, name, size);
        setStyle();
        this.craftscene=craftscene;
    }

    public UICraftPane(CraftScene craftscene,Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        setStyle();
        this.craftscene=craftscene;
    }

    @Override
    void setStyle() {
        arrow = new Image(LOG.skin.getDrawable("arrow"));
        arrow.setSize(size.x*0.12f,size.x*0.12f);
        groups = new Array<VerticalGroup>();
        itemfont= new BitmapFont(Gdx.files.internal("font.fnt"));
        itemstyle= new Label.LabelStyle();
        itemstyle.font=itemfont;
        countfont= new BitmapFont(Gdx.files.internal("font.fnt"));
        countstyle= new Label.LabelStyle();
        countstyle.font=countfont;
        rewardstyle= new Label.LabelStyle();
        rewardfont= new BitmapFont(Gdx.files.internal("font.fnt"));
        rewardfont.getData().markupEnabled=true;
        formatter=new Formatting();
        rewardstyle.font=rewardfont;
        t=new Table();
        t.center().left();
        container=new Container<Table>(t);
        container.setBackground(new NinePatchDrawable(LOG.skin.getPatch("progressbg")));
        container.fill(1,1);
        actor=container;
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        itemfont.getData().setScale(size.x/1000*ratio);
        countfont.getData().setScale(size.x/600*ratio);
        for (VerticalGroup g: groups) {
            g.padLeft(g.getPadLeft()*ratio);
            g.padRight(g.getPadRight()*ratio);
        }
        loadrecipe(recipe);
    }

    public void loadrecipe(Recipes recipe){
        t.clear();
        t.add().colspan(3).expandY();
        t.row();
        ItemStack[]stacks = recipe.getMaterial();
        ItemStack item = recipe.getItem();
        for (ItemStack stack:stacks) {
            VerticalGroup group=new VerticalGroup();
            group.padLeft( table.getCell(actor).getPrefWidth()*0.02f);
            Label itemlbl= new Label(stack.getItem().getName(),itemstyle);
            Image img = new Image(stack.getItem().getImage());
            img.setScale(0.9f);
            Container<Image>container=new Container<Image>(img);
            container.setBackground(new NinePatchDrawable(LOG.skin.getPatch("progressbg")));
            Label countlbl= new Label("x"+stack.getCount(),countstyle);

            group.addActor(itemlbl);
            group.addActor(container);
            container.size(table.getCell(actor).getPrefHeight()*0.3f,table.getCell(actor).getPrefHeight()*0.3f);
            img.setOrigin(container.getPrefWidth()/2,container.getPrefHeight()/2);
            group.addActor(countlbl);
            groups.add(group);

            t.add(group);

        }

        t.add(arrow).expandX().right().padRight(table.getCell(actor).getPrefWidth()*0.02f);
        VerticalGroup group=new VerticalGroup();
        group.padRight(table.getCell(actor).getPrefWidth()*0.02f);
        Label itemlbl= new Label(item.getItem().getName(),itemstyle);
        Image img = new Image(item.getItem().getImage());

        Container<Image>container=new Container<Image>(img);
        container.setBackground(new NinePatchDrawable(LOG.skin.getPatch("progressbg")));
        Label countlbl= new Label("x"+item.getCount(),countstyle);

        group.addActor(itemlbl);
        img.setScale(0.9f);
        container.size(table.getCell(actor).getPrefHeight()*0.3f,table.getCell(actor).getPrefHeight()*0.3f);
        img.setOrigin(container.getPrefWidth()/2,container.getPrefHeight()/2);
        group.addActor(container);
        group.addActor(countlbl);
        groups.add(group);

        double moneyval= recipe.getMoney();
        double xpval= recipe.getXp();
        double moneyharvest= moneyval*craftscene.getPlayer().getCrafting().getmoneymult();
        double xpharvest=xpval*craftscene.getPlayer().getCrafting().getxpmult();


        rewardlabel= new Label("Reward [#00FF00]$"+formatter.formatdec(moneyharvest)+" [#00FFFF]"+formatter.formatdec(xpharvest)+"XP",rewardstyle);
        t.add(group);
        t.row();
        t.add().expandY().colspan(3);
        t.row();
        t.add(rewardlabel).colspan(3);
        this.recipe=recipe;
        rewardlabel.setVisible(false);
    }

    public Recipes getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipes recipe) {
        this.recipe = recipe;
    }

    public Label getRewardlabel() {
        return rewardlabel;
    }
}
