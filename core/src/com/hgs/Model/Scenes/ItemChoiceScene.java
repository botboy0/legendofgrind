package com.hgs.Model.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Inventory.Inventory;
import com.hgs.Model.Inventory.InventoryModel;
import com.hgs.Model.Inventory.InventorySlot;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.ItemStack;
import com.hgs.Model.LOG;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UILabel;
import com.hgs.Model.UI.UIScrollInventory;

public class ItemChoiceScene extends Scene {
    final Player player;
    final UILabel inventorylabel;
    final UIScrollInventory scrollinv;
    final UIButton cancelbtn;
    String mode;
    public boolean dragging;
    boolean pressing;
    int index;
    final Vector2 pressposstart= new Vector2();
    float pressposdist;
    public ItemChoiceScene(final LOG log) {
        super(log);
        this.type=SceneType.Itemchoice;
        table.setVisible(false);
        table.top();
        player= log.player;
        inventorylabel= new UILabel("", Align.center,table,"inventorylabel",new Vector2(LOG.oWIDTH*0.45f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.07f*((float)LOG.HEIGHT/LOG.WIDTH)),1f);
        NinePatchDrawable patch = new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        inventorylabel.getlabelcontainer().setBackground(patch);
        scrollinv= new UIScrollInventory(table,"uiscrollinventory",new Vector2(LOG.oWIDTH*0.45f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.4f*((float)LOG.HEIGHT/LOG.WIDTH)),new Vector2(4,25), (int) ((LOG.oWIDTH*0.45f*((float)LOG.HEIGHT/LOG.WIDTH)/4)));
        cancelbtn= new UIButton("Cancel",table,patch,"cancel",new Vector2(LOG.oWIDTH*0.45f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH)),0.5f,new int[]{0,0,0,0});
        table.add().height(LOG.HEIGHT/7.5f);
        table.row();
        table.add(inventorylabel.getlabelcontainer());
        table.row();
        table.add(scrollinv.getActor());
        table.row();
        table.add(cancelbtn.getButton());
        components.add(inventorylabel,scrollinv,cancelbtn);
        cancelbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
               hide();
            }
        });
        player.getInventorymodel().addInventory(scrollinv.getinventory());


    }

    public void update(){
        if (pressing) {
            pressposdist= (float) Math.sqrt(Math.pow(pressposstart.x-Gdx.input.getX(),2)+Math.pow(pressposstart.y-Gdx.input.getY(),2));
            if (pressposdist>scrollinv.getSlotsize()/10f)dragging=true;
        }
        if(!pressing&&Gdx.input.isTouched())press(Gdx.input.getX(),Gdx.input.getY());
        if (pressing&&!Gdx.input.isTouched())release(Gdx.input.getX(),Gdx.input.getY());
    }

    public void press(int x, int y){
        pressing=true;
           index = findslot(x,y);
        if (index==-1)return;
        pressposstart.set(x,y);
    }

    public void release(int x,int y){
        if (dragging){
            pressing=false;
            dragging=false;
            return;
        }
            InventoryModel toswap=null;
            int  index = findslot(x,y);
            if (index==-1)return;

                ItemStack clicked=player.getInventorymodel().getStacks().get(index);
                String category=null;
                if(clicked.getItem()!=null)category= clicked.getItem().getData().getString("category","undefined");
                if (category!=null&&category==mode){
                    for (InventoryModel model:player.getModels()) {
                        if (model.getCategory()==null||model.getCategory()!=mode)continue;
                        toswap= model;
                    }
                    if (toswap!=null) toswap.Swap(player.getInventorymodel(),0,index);
                    hide();
                }

                if (clicked.getItem()==null){
                    for (InventoryModel model:player.getModels()){
                        if (model.getCategory()!=mode)continue;
                         toswap=model;
                    }
                    if (toswap!=null) toswap.Swap(player.getInventorymodel(),0,index);
                }
        pressing=false;
    }

    public int findslot(int x, int y){
        for (final InventorySlot slot:scrollinv.getinventory().getSlots()){
            Vector2 slotcords=slot.localToScreenCoordinates(new Vector2(0,0));
            Rectangle slotbound = new Rectangle(slotcords.x,slotcords.y-slot.getHeight(),slot.getWidth(),slot.getHeight());
            Vector2 invcords= scrollinv.getPane().localToScreenCoordinates(new Vector2(0,0));
            Rectangle invbound = new Rectangle(invcords.x,invcords.y-scrollinv.getPane().getHeight(),scrollinv.getPane().getWidth(),scrollinv.getPane().getHeight());
            if (table.isVisible()&&invbound.contains(x,y)&&slotbound.contains(x,y)){
                int  index = slot.getIndex();
                return index;
            }
        }
        return -1;
    }

    @Override
    public void show() {
        super.show();
        inventorylabel.getLabel().setText(mode);
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public UIScrollInventory getScrollinv() {
        return scrollinv;
    }

    @Override
    public void render(float delta, Batch batch) {
        super.render(delta, batch);
        update();
    }

}
