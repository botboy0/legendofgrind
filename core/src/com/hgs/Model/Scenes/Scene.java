package com.hgs.Model.Scenes;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.LOG;
import com.hgs.Model.UI.UIComponent;


public abstract class Scene {
    final LOG log;
    final Table table;
    SceneType type;
    final Array<UIComponent>components;

    public Scene(LOG log){
        this.log=log;
        log.scenes.add(this);
        table= new Table();
        table.setBounds(0,0, LOG.WIDTH, LOG.HEIGHT);
        components= new Array<>();
    }

    public void scale(float scale) {
        table.setBounds(0,0, LOG.WIDTH, LOG.HEIGHT);
        for (com.hgs.Model.UI.UIComponent uic: components)uic.scale(scale);
    }

    public void hide(){
        table.setVisible(false);
    }

    public void show(){
        table.setVisible(true);
    }

    public Table getTable() {
        return table;
    }

    public SceneType getType() { return type; }

    public Array<UIComponent> getComponents() {
        return components;
    }

    public void render(float delta, Batch batch){
        batch.begin();
        if (table.isVisible())table.draw(batch,1f);
        batch.end();
    }
}
