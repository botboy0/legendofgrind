package com.hgs.Model.Scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Character.Skill;
import com.hgs.Model.LOG;
import com.hgs.Model.UI.UIExplorePanel;
import com.hgs.Model.UI.UILabel;
import com.hgs.Model.UI.UIProgressTitle;

public class ExploreScene extends Scene {

    final Player player;
    final Skill exploration;
    final UIProgressTitle explorationxpbar;
    final UILabel moneylabel;
    public final UIExplorePanel explorepanel;

    public ExploreScene(LOG log) {
        super(log);
        this.type=SceneType.Exploring;
        table.top();
        explorationxpbar = new UIProgressTitle(log,type,table,"explorationxpbar",new Vector2(LOG.oWIDTH,LOG.oHEIGHT/16*((float)LOG.HEIGHT/LOG.WIDTH)), Color.CYAN,0.35f);
        player = log.player;
        exploration = player.getExploration();
        moneylabel = new UILabel("", Align.center,table,"moneylabel",new Vector2(LOG.oWIDTH/2,LOG.oHEIGHT/8),0.7f*(float)LOG.HEIGHT/LOG.WIDTH);
        explorepanel = new UIExplorePanel(log,table,"explorepanel",new Vector2(LOG.oWIDTH, LOG.oHEIGHT*0.8f*(float)LOG.HEIGHT/LOG.WIDTH));
        table.add(explorationxpbar.getActor());
        table.row();
        table.add(moneylabel.getActor());
        table.row();
        table.add().height(LOG.HEIGHT*0.025f*((float)LOG.HEIGHT/LOG.WIDTH));
        table.row();
        table.add(explorepanel.getActor());


        components.add(explorationxpbar,moneylabel,explorepanel);

    }

    private void update(float delta) {
        moneylabel.getLabel().setText("[#00ff1a]$"+LOG.formatting.formatdec(player.getbalance()));
        explorationxpbar.getProgresslabel().setText("Explore lvl: "+ LOG.formatting.formatdec(exploration.getlevel())+" - EXP: "+LOG.formatting.formatdec(exploration.getxp())+" / "+LOG.formatting.formatdec(exploration.getxpnext()));
        explorationxpbar.getPb().setValue((float) (exploration.getxp()/exploration.getxpnext()));
        explorepanel.update(delta);
    }


    @Override
    public void render(float delta, Batch batch) {
        super.render(delta, batch);
        explorepanel.render(batch);
        update(delta);
    }

}
