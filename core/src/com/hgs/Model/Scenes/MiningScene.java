package com.hgs.Model.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Character.Skill;
import com.hgs.Model.Inventory.InventoryModel;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.ItemData;
import com.hgs.Model.LOG;
import com.hgs.Model.UI.*;

public class MiningScene extends Scene {
    final Player player;
    final Skill mining;
    final UIProgressTitle miningxpbar;
    final UILabel moneylabel;
    public final UIOre ore;
    UIProgressbar orehp;
    public final UIInventory pickaxeslot;
    public final InventoryModel pickaxemodel;
    public final UIButton pickaxeinv;
    final UILabel pickaxelabel;

    Item pickaxe;
    float itemsize =LOG.WIDTH*0.08f*((float)LOG.HEIGHT/LOG.WIDTH);
    Item selected;

    public MiningScene(final LOG log) {
        super(log);
        this.type=SceneType.Mining;
        table.setVisible(false);
        table.top();
        miningxpbar= new UIProgressTitle(log,type,table,"miningxpbar",new Vector2(LOG.oWIDTH,LOG.oHEIGHT/16*((float)LOG.HEIGHT/LOG.WIDTH)), Color.CYAN,0.35f);
        player = log.player;
        mining = player.getMining();
        moneylabel = new UILabel("", Align.center,table,"moneylabel",new Vector2(LOG.oWIDTH/2,LOG.oHEIGHT/8),0.7f*(float)LOG.HEIGHT/LOG.WIDTH);
        ore = new UIOre(log,table,"oreimage",new Vector2(LOG.oHEIGHT/3*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT/3*((float)LOG.HEIGHT/LOG.WIDTH)));
        orehp = new UIProgressbar(table,"stonehp",new Vector2(LOG.oHEIGHT/4*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT/32*((float)LOG.HEIGHT/LOG.WIDTH)),Color.GRAY,0.35f);
        pickaxelabel = new UILabel("Pickaxe", Align.center,table,"seedlbl",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH)),2f);
        pickaxeslot = new UIInventory(table,"pickaxeslot",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.12f*((float)LOG.HEIGHT/LOG.WIDTH)),new Vector2(1,1), (int) (LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH)));
        pickaxeinv = new UIButton("",table,LOG.skin.getDrawable("inventory"),"pickaxeinv",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH)),new int[]{LOG.oHEIGHT/30,0,0,0});

        table.add(miningxpbar.getActor());
        table.row();
        table.add(moneylabel.getlabelcontainer());
        table.row();
        table.add().height(LOG.HEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH));
        table.row();
        table.add(ore.getOreimage());
        table.row();
        table.add(orehp.getActor());
        table.row();
        table.add().height(LOG.HEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH));
        table.row();
        table.add(pickaxelabel.getlabelcontainer());
        table.row();
        table.add(pickaxeslot.getActor());
        table.row();
        table.add(pickaxeinv.getButton());

        components.add(miningxpbar,moneylabel,ore, orehp);
        components.add(pickaxelabel,pickaxeslot,pickaxeinv);
        pickaxemodel= player.getPickaxemodel();
        pickaxemodel.addInventory(pickaxeslot.getinventory());

        pickaxeinv.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                log.itemchoicescene.setMode("Pickaxe");
                log.itemchoicescene.show();
            }
        });


        pickaxeslot.getinventory().getSlots().get(0).addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (pickaxemodel.getStacks().get(0)!=null)
                    selected= pickaxemodel.getStacks().get(0).getItem();
                return true;
            }
        });



    }

    private void update(float delta) {
        moneylabel.getLabel().setText("[#00ff1a]$"+LOG.formatting.formatdec(player.getbalance()));
        miningxpbar.getProgresslabel().setText("Mining lvl: "+ LOG.formatting.formatdec(mining.getlevel())+" - EXP: "+LOG.formatting.formatdec(mining.getxp())+" / "+LOG.formatting.formatdec(mining.getxpnext()));
        miningxpbar.getPb().setValue((float) (mining.getxp()/mining.getxpnext()));
        ItemData oredata = ore.getOre().getData();
        float hp = oredata.getFloat("hp",-1);
        float hpmax = oredata.getFloat("hpmax",-1);
        orehp.getProgresslabel().setText(ore.getOre().getName()+" "+(int)Math.ceil(hp)+" / "+(int)Math.ceil(hpmax)+" HP");
        orehp.getPb().setValue(hp/hpmax);

        if (Gdx.input.isTouched()){
            Vector2 touch = new Vector2(Gdx.input.getX(),Gdx.graphics.getHeight()-Gdx.input.getY());
            if (getSelected()!=null){
                TextureRegion selected= getSelected().getImage();
                LOG.batch.begin();
                LOG.batch.draw(selected, Gdx.input.getX() - itemsize / 2, LOG.HEIGHT - Gdx.input.getY() - itemsize / 2, itemsize, itemsize);
                LOG.batch.end();
                if (ore.getbounds().contains(touch)) {
                    double dist = Math.sqrt(Math.pow(Gdx.input.getDeltaX(), 2) + Math.pow(Gdx.input.getDeltaY(), 2));
                   if(ore.mine(dist,getSelected().getData().getFloat("damage",0))){
                       float durability=getSelected().getData().getFloat("durability",-1)-1;
                       getSelected().getData().putFloat("durability",durability);
                       pickaxemodel.sync();
                       if (durability<=0){
                           pickaxemodel.RemoveItems(getSelected(),1);
                           setSelected(null);
                       }
                   }
                }
            }
        }else {
            setSelected(null);
        }
        ore.update(delta);
    }

    @Override
    public void render(float delta, Batch batch) {
        super.render(delta, batch);
        update(delta);
    }

    @Override
    public void scale(float scale) {
        super.scale(scale);
    }

    public Item getSelected() {
        return selected;
    }

    public void setSelected(Item selected) {
        this.selected = selected;
    }
}
