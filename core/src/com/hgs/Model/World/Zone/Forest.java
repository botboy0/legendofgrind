package com.hgs.Model.World.Zone;

import com.hgs.Model.World.LootTable;

public class Forest extends Zone{
    public Forest() {
        super("Forest", new LootTable(new String[]{"Wood"},new float[]{0}), 10,1,1);
    }
}
