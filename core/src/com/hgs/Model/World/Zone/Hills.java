package com.hgs.Model.World.Zone;

import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.Seed.CarrotSeed;
import com.hgs.Model.World.LootTable;

public class Hills extends Zone {
    public Hills() {
        super("Hills", new LootTable(new String[]{"CarrotSeed","PotatoSeed","WheatSeed"},new float[]{0,1/3f,2/3f}), 15,5,5);
    }
}
