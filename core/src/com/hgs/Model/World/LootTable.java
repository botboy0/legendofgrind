package com.hgs.Model.World;

import com.badlogic.gdx.Gdx;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.Items;

import java.util.HashMap;
import java.util.Random;

public class LootTable {
    String[]lootables;
    float[]scores;


    public LootTable(String[] lootables, float[] scores){
        this.lootables=lootables;
        this.scores=scores;
    }



    public Item loot(){
        if (lootables.length==0)return null;
        Random r = new Random();
        float c = r.nextFloat();
        for (int i=0;i<lootables.length;i++){
            if (c<scores[i])return Items.valueOf(lootables[i]).getItem();
        }
        return Items.valueOf(lootables[0]).getItem();
    }

    public Item[] getItems(){
        Item[]items= new Item[lootables.length];
        for (int i=0;i<lootables.length;i++)items[i]=Items.valueOf(lootables[i]).getItem();
        return items;
    }

}
