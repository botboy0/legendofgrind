package com.hgs.Model.Util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class Formatting {
    private final DecimalFormat formatter;

    public Formatting(){
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        otherSymbols.setDecimalSeparator('.');
            formatter= new DecimalFormat("#.##", otherSymbols);
        }

        public String formatdec(double d){
            return formatter.format(d);
        }
}
